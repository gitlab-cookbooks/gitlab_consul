#
# Cookbook:: gitlab_consul
# Spec:: service
#
# Copyright:: 2017, The Authors, All Rights Reserved.

require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab_consul::agent_cleanup' do
  context 'When all attributes are default, on an unspecified platform' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end

  context 'When agent_cleanup is enabled' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new do |node|
        node.normal['gitlab_consul']['agent_cleanup']['enabled'] = 'true'
      end
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      Dir.stub(:exist?).and_return(false)
      expect { chef_run }.to_not raise_error
    end
  end
end
