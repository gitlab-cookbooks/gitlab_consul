class AddressDetector
  attr_reader :node, :scope

  def initialize(node, scope)
    @node = node
    @scope = check_scope(scope)
  end

  def ipaddress
    @ip ||= interface['addresses'].detect { |_k, v| v['family'] == 'inet' }.first
  end

  private

  def interface
    return specific_interface if bind_interface

    interfaces.select { |_k, v| v['encapsulation'] == 'Ethernet' }.values.first
  end

  def specific_interface
    raise "Interface '#{bind_interface}' doesn't exists" unless interfaces[bind_interface]

    interfaces[bind_interface]
  end

  def interfaces
    node['network']['interfaces']
  end

  def bind_interface
    node['gitlab_consul'][scope]['bind_interface'] if node['gitlab_consul'] && node['gitlab_consul'][scope]
  end

  def check_scope(scope)
    raise "Unkown scope '#{scope}'. Expected 'client' or 'cluster'" unless scope.match /(client|cluster)/
    @scope = scope
  end
end
