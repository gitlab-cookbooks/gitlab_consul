class Chef
  class Resource
    class SslCertificate < Chef::Resource
      # HACK: Chef 16 drops support for declaring unnamed providers.
      # We need to explicitly let Chef know this resource declares the `ssl_certificate` provider now.
      # The long term solution is to switch to using: https://docs.chef.io/resources/openssl_x509_certificate/
      if Chef::VERSION.to_i >= 16
        provides :ssl_certificate
      end
    end
  end
end
