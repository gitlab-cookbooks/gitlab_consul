node['gitlab_consul']['services'].each do |service|
  params = {}
  %i(address tags check checks).each do |param|
    value = service[param.to_s]
    params[param] = value if value
  end
  %i(port).each do |param|
    value = service[param.to_s].to_i
    params[param] = value if value
  end

  consul_definition service['name'] do
    type 'service'
    parameters(params)
    if node['gitlab_consul']['agent']['enabled']
      notifies :reload, 'consul_service[consul]', :delayed
    end
  end
end
