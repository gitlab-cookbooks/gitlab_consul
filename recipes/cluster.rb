# Build a Consul Cluster

# Enable Prometheus monitoring.
node.default['consul']['config']['telemetry'] = {
  disable_hostname: true,
  prometheus_retention_time: '768h', # 32d
}

# Use Prometheus node labels as node metadata.
node_meta = node['prometheus']['labels'].merge({ 'fqdn': node['fqdn'] })
node.default['consul']['config']['node_meta'] = node_meta

poise_service_user node['consul']['service_user'] do
  group node['consul']['service_group']
end

directory File.dirname(node['gitlab_consul']['cluster']['tls']['ssl_key']['path']) do
  recursive true
  owner node['consul']['service_user']
  group node['consul']['service_group']
  action :create
end

directory File.dirname(node['gitlab_consul']['cluster']['tls']['ssl_cert']['path']) do
  recursive true
  owner node['consul']['service_user']
  group node['consul']['service_group']
  action :create
end

# ssl_key
ssl_key_secrets = get_secrets(node['gitlab_consul']['cluster']['tls']['ssl_key']['secret_source'],
  node['gitlab_consul']['cluster']['tls']['ssl_key']['secret_bag'],
  node['gitlab_consul']['cluster']['tls']['ssl_key']['item'])
node.default['gitlab_consul']['cluster']['tls']['ssl_key']['content'] = ssl_key_secrets[node['gitlab_consul']['cluster']['tls']['ssl_key']['item_key']]
# ssl_certs
ssl_cert_secrets = get_secrets(node['gitlab_consul']['cluster']['tls']['ssl_cert']['secret_source'],
    node['gitlab_consul']['cluster']['tls']['ssl_cert']['secret_bag'],
    node['gitlab_consul']['cluster']['tls']['ssl_cert']['item'])
node.default['gitlab_consul']['cluster']['tls']['ssl_cert']['content'] = ssl_cert_secrets[node['gitlab_consul']['cluster']['tls']['ssl_cert']['item_key']]
# ssl_chain
ssl_chain_secrets = get_secrets(node['gitlab_consul']['cluster']['tls']['ssl_chain']['secret_source'],
    node['gitlab_consul']['cluster']['tls']['ssl_chain']['secret_bag'],
    node['gitlab_consul']['cluster']['tls']['ssl_chain']['item'])
node.default['gitlab_consul']['cluster']['tls']['ssl_chain']['content'] = ssl_chain_secrets[node['gitlab_consul']['cluster']['tls']['ssl_chain']['item_key']]

certificate = ssl_certificate node['consul']['service_name'] do
  owner node['consul']['service_user']
  group node['consul']['service_group']
  namespace node['gitlab_consul']['cluster']['tls']
  notifies :reload, "consul_service[#{name}]", :delayed
end

node.default['consul']['config']['server'] = true
node.default['consul']['config']['verify_incoming'] = false
node.default['consul']['config']['verify_outgoing'] = false

ad = AddressDetector.new(node, 'cluster')

node.default['consul']['config']['bind_addr'] = ad.ipaddress
node.default['consul']['config']['advertise_addr'] = ad.ipaddress
node.default['consul']['config']['advertise_addr_wan'] = ad.ipaddress

node.default['consul']['config']['ca_file'] = certificate.chain_path
node.default['consul']['config']['cert_file'] = certificate.cert_path
node.default['consul']['config']['key_file'] = certificate.key_path
node.default['consul']['config']['start_join'] = node['gitlab_consul']['cluster_nodes']
node.default['consul']['config']['enable_local_script_checks'] = true

node.default['consul']['config']['disable_update_check'] = true

consul_definition 'consul' do
  type 'services'
  parameters [
    {
      id: 'consul-leader',
      name: 'consul',
      tags: [
        'leader',
      ],
      checks: [
        {
          args: [
            '/bin/sh',
            '-c',
            "/usr/local/bin/consul info | grep 'leader = true' || exit 2",
          ],
          interval: '10s',
        },
      ],
    },
    {
      id: 'consul-follower',
      name: 'consul',
      tags: [
        'follower',
      ],
      checks: [
        {
          args: [
            '/bin/sh',
            '-c',
            "/usr/local/bin/consul info | grep 'leader = false' || exit 2",
          ],
          interval: '10s',
        },
      ],
    },
  ]
  notifies :reload, 'consul_service[consul]', :delayed
end

include_recipe 'consul::default'

poise_service_options node['consul']['service_name'] do
  for_provider :systemd
  template 'gitlab_consul:systemd.service.erb'
  systemd_settings node['gitlab_consul']['systemd_settings']
end
