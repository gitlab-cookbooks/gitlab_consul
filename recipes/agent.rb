# Install the Consul Agent
#

ad = AddressDetector.new(node, 'client')
node.default['consul']['config']['bind_addr'] = ad.ipaddress
node.default['consul']['config']['telemetry'] = {
  disable_hostname: true,
  prometheus_retention_time: '768h', # 32d
}

node.override['consul']['config']['start_join'] = node['gitlab_consul']['cluster_nodes']
node.override['consul']['config']['datacenter'] = node['gitlab_consul']['datacenter']
node.override['consul']['config']['encrypt'] = node['gitlab_consul']['encrypt']
node.override['consul']['config']['acl_token'] = node['gitlab_consul']['acl_token']

# Use Prometheus node labels as node metadata.
node_meta = node['prometheus']['labels'].merge({ 'fqdn': node['fqdn'] })
node.default['consul']['config']['node_meta'] = node_meta

poise_service_user node['consul']['service_user'] do
  group node['consul']['service_group']
  shell node['gitlab_consul']['agent']['shell']
end

directory File.dirname(node['gitlab_consul']['client']['tls']['ssl_key']['path']) do
  recursive true
  owner node['consul']['service_user']
  group node['consul']['service_group']
  action :create
end

directory File.dirname(node['gitlab_consul']['client']['tls']['ssl_cert']['path']) do
  recursive true
  owner node['consul']['service_user']
  group node['consul']['service_group']
  action :create
end

if node['gitlab_consul']['client']['secrets_source'] == 'hashicorp_vault'
  ssl_client_secrets = get_secrets('hashicorp_vault',
    node['gitlab_consul']['client']['secrets_path'])

  node.default['gitlab_consul']['client']['tls']['ssl_key']['content'] = ssl_client_secrets[node['gitlab_consul']['client']['tls']['ssl_key']['item_key']]
  node.default['gitlab_consul']['client']['tls']['ssl_cert']['content'] = ssl_client_secrets[node['gitlab_consul']['client']['tls']['ssl_cert']['item_key']]
  node.default['gitlab_consul']['client']['tls']['ssl_chain']['content'] = ssl_client_secrets[node['gitlab_consul']['client']['tls']['ssl_chain']['item_key']]
else
  ssl_key_secrets = get_secrets(node['gitlab_consul']['client']['tls']['ssl_key']['secret_source'],
    node['gitlab_consul']['client']['tls']['ssl_key']['secret_bag'],
    node['gitlab_consul']['client']['tls']['ssl_key']['item'])

  ssl_cert_secrets = get_secrets(node['gitlab_consul']['client']['tls']['ssl_cert']['secret_source'],
    node['gitlab_consul']['client']['tls']['ssl_cert']['secret_bag'],
    node['gitlab_consul']['client']['tls']['ssl_cert']['item'])

  ssl_chain_secrets = get_secrets(node['gitlab_consul']['client']['tls']['ssl_chain']['secret_source'],
    node['gitlab_consul']['client']['tls']['ssl_chain']['secret_bag'],
    node['gitlab_consul']['client']['tls']['ssl_chain']['item'])

  node.default['gitlab_consul']['client']['tls']['ssl_key']['content'] = ssl_key_secrets[node['gitlab_consul']['client']['tls']['ssl_key']['item_key']]
  node.default['gitlab_consul']['client']['tls']['ssl_cert']['content'] = ssl_cert_secrets[node['gitlab_consul']['client']['tls']['ssl_cert']['item_key']]
  node.default['gitlab_consul']['client']['tls']['ssl_chain']['content'] = ssl_chain_secrets[node['gitlab_consul']['client']['tls']['ssl_chain']['item_key']]
end

certificate = ssl_certificate node['consul']['service_name'] do
  owner node['consul']['service_user']
  group node['consul']['service_group']
  namespace node['gitlab_consul']['client']['tls']
  if node['gitlab_consul']['agent']['enabled']
    notifies :reload, "consul_service[#{name}]", :delayed
  end
end

node.default['consul']['config']['ca_file'] = certificate.chain_path
node.default['consul']['config']['cert_file'] = certificate.cert_path
node.default['consul']['config']['key_file'] = certificate.key_path

node.default['consul']['config']['verify_outgoing'] = false

node.default['consul']['config']['disable_update_check'] = true
node.default['consul']['config']['enable_local_script_checks'] = node['gitlab_consul']['client']['enable_local_script_checks']

if node['gitlab_consul']['agent']['enabled']
  include_recipe 'consul::default'

  poise_service_options node['consul']['service_name'] do
    for_provider :systemd
    template 'gitlab_consul:systemd.service.erb'
    systemd_settings node['gitlab_consul']['systemd_settings']
  end
end
