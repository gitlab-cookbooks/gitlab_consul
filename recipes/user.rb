# Create the consul user, and do nothing else

poise_service_user node['consul']['service_user'] do
  group node['consul']['service_group']
end
