# Cleanup old versions of the Consul Agent
#
if node['gitlab_consul']['agent_cleanup']['enabled'] && Dir.exist?(node['gitlab_consul']['consul_prefix_path'])
  ruby_block 'get list of directories to remove' do
    block do
      # get list of all dirs
      all_dirs = Dir.entries(node['gitlab_consul']['consul_prefix_path'])
      # get list of all non-required dirs
      node.run_state['dirs_to_delete'] = all_dirs - [node['consul']['version']] - ['.', '..']
      :nothing
    end
  end.run_action(:run)

  node.run_state['dirs_to_delete'].each do |mydir|
    directory node['gitlab_consul']['consul_prefix_path'] + "/#{mydir}" do
      recursive true
      action :delete
    end
  end
end
